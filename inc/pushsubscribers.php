<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Désinscrire un pushsubscriber
 */
function pushsubscribers_desinscrire($id_ou_endpoint) {
	if (is_string($id_ou_endpoint)) {
		$id_pushsubscriber = sql_getfetsel('id_pushsubscriber', 'spip_pushsubscribers', 'endpoint = '.sql_quote($id_ou_endpoint));
	}
	else {
		$id_pushsubscriber = intval($id_ou_endpoint);
	}
	
	if ($id_pushsubscriber > 0) {
		include_spip('inc/autoriser');
		
		// INSUP
		autoriser_exception('modifier', 'pushsubscriber', $id_pushsubscriber, true);
		autoriser_exception('instituer', 'pushsubscriber', $id_pushsubscriber, true);
		// On met à la poubelle, et le nettoyage des choses liées sera plus tard
		objet_modifier('pushsubscriber', $id_pushsubscriber, array(
			'statut' => 'poubelle',
		));
		autoriser_exception('modifier', 'pushsubscriber', $id_pushsubscriber, false);
		autoriser_exception('instituer', 'pushsubscriber', $id_pushsubscriber, false);
	}
}

/**
 * Inscrire ou désinscrire à une liste précise, et la créer dans la base si non existante
 * 
 * @param $id_ou_endpoint
 * 		ID SQL ou endpoint d'un pushsubscribers
 * @param string $contexte
 * 		Chaine identifiant un contexte de liste, peut contenir une query string
 * @param bool $inscrire
 * 		Booléen indiquant si on doit inscrire ou sinon désinscrire à cette liste
 */
function pushsubscribers_switch_pushlist($id_ou_endpoint, $contexte, $inscrire=true) {
	include_spip('action/editer_objet');
	$id_pushsubscriber = 0;
	
	// On vérifie que le pushsubscriber existe bien
	if (is_numeric($id_ou_endpoint)) {
		// un id qui peut aussi être une chaine "123"
		$id_pushsubscriber = sql_getfetsel('id_pushsubscriber', 'spip_pushsubscribers', 'id_pushsubscriber = '.intval($id_ou_endpoint));
	}
	elseif (is_string($id_ou_endpoint)) {
		// ou vraiment une chaine
		$id_pushsubscriber = sql_getfetsel('id_pushsubscriber', 'spip_pushsubscribers', 'endpoint = '.sql_quote($id_ou_endpoint));
	}
	
	// Seulement si on a bien trouvé un inscrit
	if ($id_pushsubscriber = intval($id_pushsubscriber)) {
		// Cette liste existe-t-elle déjà ?
		if (!$id_pushlist = sql_getfetsel('id_pushlist', 'spip_pushlists', 'contexte = '.sql_quote($contexte))) {
			// On crée la liste
			$id_pushlist = objet_inserer('pushlist');
			// Et en modifiant son contexte, c'est censé pré-remplir les autres bonnes infos
			objet_modifier('pushlist', $id_pushlist, array('contexte' => $contexte));
		}
		
		// Si on a donc bien une liste
		if ($id_pushlist) {
			// Si on veut inscrire et qu'il n'y a pas déjà l'inscription
			if (
				$inscrire
				and !$pushsubscription = sql_fetsel(
					'*',
					'spip_pushsubscriptions',
					array(
						'id_pushsubscriber = ' . $id_pushsubscriber,
						'id_pushlist = ' . $id_pushlist,
					)
				)
			) {
				$id_pushsubscription = sql_insertq(
					'spip_pushsubscriptions',
					array(
						'id_pushsubscriber' => $id_pushsubscriber,
						'id_pushlist' => $id_pushlist,
					)
				);
			}
			// Sinon si on veut désinscrire
			elseif (!$inscrire) {
				sql_delete(
					'spip_pushsubscriptions',
					array(
						'id_pushsubscriber = ' . $id_pushsubscriber,
						'id_pushlist = ' . $id_pushlist,
					)
				);
			}
		}
	}
}

/**
 * Lance le chargeur PSR pour les libs embarquées
 **/
function pushsubscribers_loader() {
	static $done = false;
	
	if (!$done) {
		$done = true;
		require_once __DIR__ . '/../vendor/autoload.php';
	}
}

/**
 * Transformer notre objet SPIP en tableau suivant la norme attendue puis en JSON
 */
function pushsubscribers_preparer_payload($id_ou_push) {
	$payload = null;
	
	if (is_numeric($id_ou_push) and $id_push = intval($id_ou_push)) {
		$push = sql_fetsel('*', 'spip_pushs', 'id_push = '.$id_push);
	}
	else {
		$push = $id_ou_push;
	}
	
	if (is_array($push)) {
		// TODO
		$payload = array(
			'title' => $push['titre'],
			'body' => $push['texte'],
			//'icon' => 'URL image',
			'data' => array(
				'url' => $push['url'] ? $push['url'] : $GLOBALS['meta']['adresse_site'],
			),
			'actions' => array(
				array('action' => 'close', 'title' => 'Fermer'),
			)
		);
		
		// Dans la norme du Protocole l'id unique qui permet de ne pas doublonner s'appelle "topic" alors que dans la norme JS Notifications, c'est "tag"
		if ($push['topic']) {
			$payload['tag'] = $push['topic'];
		}
	}
	
	return $payload;
}

/**
 * Envoyer UNE notification, en ayant toutes les infos nécessaires
 * 
 * Doit plutôt être employée pour envoyer des notifs transactionnelles (suivi, admin), pas pour de l'information en masse
 * 
 * @param array $subscriber
 * 		Tableau contenant les infos d'un inscrit :
 *    array(
 * 			'endpoint' => …
 * 			'keys' => array(
 * 				'auth' => …
 * 				'p256dh' => …
 * 			)
 * 		)
 * @param string $payload
 * 		Chaine représentant un objet JSON ou tableau PHP, avec le contenu attendu par le service-worker :
 * 		array(
 * 			'title' => 'Titre de la notif',
 * 			'message' => 'Contenu texte de la notif',
 * 			'icon' => 'URL image',
 * 			'data' => array('url' => 'URL de lien quand on clique',),
 * 			'actions' => array(
 * 				array('action' => 'close', 'title' => 'Fermer'),
 * 			)
 * 		)
 */
function pushsubscribers_envoyer_notification($pushsubscriber, $payload) {
	include_spip('base/abstract_sql');
	$retour = null;
	
	// Si on a bien des infos d'inscription
	if (
		(is_int($pushsubscriber) and $pushsubscriber = sql_fetsel('*', 'spip_pushsubscribers', 'id_pushsubscriber = '.$pushsubscriber))
		or
		(is_array($pushsubscriber) and isset($pushsubscriber['endpoint']) and isset($pushsubscriber['auth']) and isset($pushsubscriber['p256dh']))
	) {
		// On charge pour les classes
		pushsubscribers_loader();
		
		// On crée le controleur
		$webpush = new Minishlink\WebPush\WebPush(array(
			'VAPID' => array(
				'subject' => lire_config('pushsubscribers/vapid_subject'),
				'publicKey' => lire_config('pushsubscribers/vapid_public'),
				'privateKey' => lire_config('pushsubscribers/vapid_private'),
			)
		));
		
		// On crée vraiment l'inscription en objet
		$subscription = Minishlink\WebPush\Subscription::create(array(
			'endpoint' => $pushsubscriber['endpoint'],
			'keys' => array(
				'auth' => $pushsubscriber['auth'],
				'p256dh' => $pushsubscriber['p256dh'],
			),
		));
		
		if (is_array($payload)) {
			$payload = json_encode($payload);
		}
		
		// On envoie la notif
		//~ var_dump(lire_config('pushsubscribers/vapid_public'), lire_config('pushsubscribers/vapid_private'), $webpush, $subscription);
		$report = $webpush->sendOneNotification($subscription, $payload);
		
		// On teste si ça a marché
		if ($report->isSuccess()) {
			//~ var_dump('yeah ! :D');
		}
		else {
			//~ var_dump('caca :(', $report->getReason(), $report->getRequest(), $report->getResponse(), $report->isSubscriptionExpired());
			$retour = $report->getReason();
		}
	}
	
	return $retour;
}

/**
 * Definir la combinaison (période, nb envois) pour respecter la cadence maxi configurée
 *
 * Pour l'instant toujours au max, car pas de limite connue
 * 
 * @return array
 *   (période du cron, nb envois à chaque appel)
 */
function pushsubscribers_cadence() {
	return array(30, 100); // toutes les 30s
}

/**
 * Envoyer le maximum possible de notifications par lot
 */
function pushsubscribers_envoyer_lot($nb_max=5) {
	include_spip('base/abstract_sql');
	
	$nb_restant = $nb_max;
	$now = $_SERVER['REQUEST_TIME'] ?? time();
	define('PUSHSUBSCRIBERS_MAX_TIME', $now+25); // 25s maxi
	define('PUSHSUBSCRIBERS_MAX_TRY', 5); // 5 essais maxis par destinataires
	
	// On traite au maximum 2 séries d'envois dans un appel
	if ($pushs = sql_allfetsel('*', 'spip_pushs', 'statut = '.sql_quote('processing'), '', 'id_push','0,2')) {
		// On charge pour les classes
		pushsubscribers_loader();
		
		// On crée le controleur
		$webpush = new Minishlink\WebPush\WebPush(array(
			'VAPID' => array(
				'subject' => lire_config('pushsubscribers/vapid_subject'),
				'publicKey' => lire_config('pushsubscribers/vapid_public'),
				'privateKey' => lire_config('pushsubscribers/vapid_private'),
			)
		));
		
		foreach ($pushs as $push) {
			// On s'arrête si on n'a déjà plus le temps
			if (time() > PUSHSUBSCRIBERS_MAX_TIME) {
				return $nb_restant;
			}
			
			// On prépare le tableau au bon format de la norme suivant le contenu de l'objet SPIP
			$payload = pushsubscribers_preparer_payload($push);
			
			// chercher les N prochains destinataires
			if ($destinataires = sql_allfetsel(
				'*',
				'spip_pushs_destinataires',
				'id_push = '.intval($push['id_push']).' AND statut = '.sql_quote('todo'),
				'',
				'try',
				"0,$nb_max"
			)) {
				// On remplit les choses envoyer
				foreach ($destinataires as $cle=>$destinataire) {
					$webpush->queueNotification(
						Minishlink\WebPush\Subscription::create(array(
							'endpoint' => $destinataire['endpoint'],
							'keys' => array(
								'auth' => $destinataire['auth'],
								'p256dh' => $destinataire['p256dh'],
							),
						)),
						// String en JSON
						json_encode($payload),
						// Options
						array(
							// Le topic/tag si défini
							'topic' => isset($payload['tag']) ?  $payload['tag'] : null,
						)
					);
					
					// On change la clé pour le endpoint
					$destinataires[$destinataire['endpoint']] = $destinataire;
					unset($destinataires[$cle]);
				}
				
				// Puis on envoie et on vérifie chaque envoi
				foreach ($webpush->flush() as $report) {
					$endpoint = $report->getEndpoint();
					$try = $destinataires[$endpoint]['try'] + 1;
					
					if ($report->isSuccess()) {
						$maj = array(
							'statut' => 'sent',
							'try' => $try,
							'date' => date('Y-m-d H:i:s'),
						);
					}
					else {
						$raison = $report->getReason();
						$expire = $report->isSubscriptionExpired();
						
						// Cas où ça a vraiment trop merdé
						if ($expire or $try >= PUSHSUBSCRIBERS_MAX_TRY) {
							$maj = array(
								'statut' => 'fail',
								'try' => $try,
								'date' => date('Y-m-d H:i:s'),
							);
							
							// Si c'est clairement annoncé comme expiré, il faut désinscrire
							if ($expire) {
								pushsubscribers_desinscrire($endpoint);
							}
						}
						// Sinon on retentera plus tard
						else {
							$maj = array(
								'try' => $try,
								'date' => date('Y-m-d H:i:s'),
							);
						}
					}
					
					if ($maj) {
						// On met à jour le destinataire
						sql_updateq('spip_pushs_destinataires', $maj, 'id_push = '.intval($push['id_push']).' AND endpoint = '.sql_quote($endpoint));
					}
					
					$nb_max--;
				}
				
				// si $nb_max non nul verifier qu'il n'y a plus de dests sur cette envoi pour maj le statut juste en dessous
				if ($nb_max) {
					//~ $nb_destinataires = sql_countsel('spip_pushs_destinataires', 'id_push = '.intval($push['id_push']).' AND statut='.sql_quote('todo'), '', 'try', "0,$nb_max");
					$dests = sql_allfetsel("*","spip_pushs_destinataires","id_push=".intval($push['id_push'])." AND statut=".sql_quote('todo'),'','try',"0,$nb_max");
				}
			}
			
			// Plus de destinataires ? on a fini, on met à jour compteur et statut
			if ($nb_max and !count($dests)) {
				$set = array(
					'statut' => 'end',
					'date' => date('Y-m-d H:i:s'),
				);
				sql_updateq('spip_pushs', $set, 'id_push = '.intval($push['id_push']));
				
				//~ pushsubscribers_compter_envois($push['id_push']);
				pushsubscribers_update_meta_processing();
			}
			
			if (!$nb_max or time() > PUSHSUBSCRIBERS_MAX_TIME) {
				return $nb_restant;
			}
		}
	}
}

/**
 * Mettre a jour la meta qui indique qu'au moins un envoi est en cours
 * 
 * Évite un acces SQL à chaque hit du cron
 *
 * @param bool $force
 * @return bool
 */
function pushsubscribers_update_meta_processing($force = false) {
	$current = (
		(isset($GLOBALS['meta']['pushsubscribers_processing']) and $GLOBALS['meta']['pushsubscribers_processing'])
		? $GLOBALS['meta']['pushsubscribers_processing']
		: false
	);

	$new = false;
	
	// S'il y a au moins un push déjà en cours d'envoi
	if ($force or sql_countsel('spip_pushs', 'statut = '.sql_quote('processing'))) {
		$new = 'oui';
	}
	
	// Ou s'il y a un push pour plus tard
	if ($new === false and $next = sql_getfetsel('date_debut', 'spip_pushs', 'statut = '.sql_quote('init'), '', 'date_debut', '0,1')) {
		$new = strtotime($next);
		
		// S'il y a encore le temps on pourrait relancer de suite
		if ($new > $_SERVER['REQUEST_TIME']){
			$new = 'oui';
		}
	}
	
	if ($new or $new !== $current){
		include_spip('inc/meta');
		
		if ($new) {
			ecrire_meta("pushsubscribers_processing", $new);
			// reprogrammer le cron
			include_spip('inc/genie');
	    genie_queue_watch_dist();
		}
		else {
			effacer_meta('pushsubscribers_processing');
		}
	}

	return $new;
}

/**
 * Lit le contenu d'un flux d'une pushlist, et programme les envois de chaque push
 * 
 * Si pas de pushlist fournie, choisit la liste ayant un flux, la plus proche à mettre à jour
 * 
 * @return int
 *   Retourne 0 si rien à faire
 */
function pushsubscribers_pushlist_programmer_feed($id_pushlist = null) {
	$id_pushlist = intval($id_pushlist);
	
	// On ne vérifie pas plus que toutes les 2h par défaut
	define('PUSHSUBSCRIBERS_FEED_PERIODE', 2*60); // en minutes
	
	// Chercher la liste à mettre à jour
	if (!$id_pushlist) {
		$jourdhui = date('Y-m-d H:i:s');
		$pushlist = sql_fetsel(
			'id_pushlist, contexte, date_feed',
			'spip_pushlists',
			array(
				// seulement les flux auto
				'type = "feed"',
				// qui n'ont pas déjà été mis à jour récemment
				array('not', sql_date_proche('date_feed', (0 - PUSHSUBSCRIBERS_FEED_PERIODE), "MINUTE"))
			),
			'',
			'date_feed ASC'
		);
	}
	else {
		$pushlist = sql_fetsel(
			'id_pushlist, contexte, date_feed',
			'spip_pushlists',
			'id_pushlist = '.$id_pushlist
		);
	}
	
	// Si on a bien une liste à travailler
	if ($pushlist) {
		$id_pushlist = intval($pushlist['id_pushlist']);
		$contexte = $pushlist['contexte'];
		$date_feed = $pushlist['date_feed'];
		// Si ya une dernière date, on l'ajoute au contexte
		if ($date_feed != '0000-00-00 00:00:00') {
			$contexte = parametre_url($contexte, 'date_feed', $date_feed, '&');
		}
		$fonction_pushlist = charger_fonction('pushlist', 'pushs');
		
		// Si on a bien un flux
		if ($feed = $fonction_pushlist($contexte) and isset($feed['pushs']) and is_array($feed['pushs'])) {
			include_spip('action/editer_objet');
			include_spip('inc/autoriser');
			
			foreach ($feed['pushs'] as $push) {
				// Ce nouveau push sera forcément à envoyer direct, pas en brouillon
				$push['statut'] = 'init';
				
				// Par défaut on n'insère pas deux fois le même topic, on cherche si ça existe déjà
				if (isset($push['topic']) and $push['topic'] and (!defined('PUSHSUBSCRIBERS_TOPIC_UNIQUE') or PUSHSUBSCRIBERS_TOPIC_UNIQUE)) {
					$id_push = sql_getfetsel('id_push', 'spip_pushs', 'topic = '.sql_quote($push['topic']));
				}
				
				// Si pas trouvé on en crée un nouveau
				if (!$id_push) {
					$id_push = objet_inserer('push');
				}
				
				// Dans tous les cas, la dernière version du contenu de ce push prend la place
				autoriser_exception('modifier', 'push', $id_push, true);
				autoriser_exception('instituer', 'push', $id_push, true);
				objet_modifier('push', $id_push, $push);
				autoriser_exception('instituer', 'push', $id_push, false);
				autoriser_exception('modifier', 'push', $id_push, false);
				
				// Si on a bien un push au final, il faut maintenant programmer son envoi à la liste
				if ($id_push) {
					job_queue_add(
						'pushsubscribers_pushlist_programmer_push',
						"Programmer l’envoi du push $id_push à la liste $id_pushlist",
						array($id_pushlist, $id_push),
						'inc/pushsubscribers',
						true
					);
					
					// On vérifie s'il y a des envois à faire
					pushsubscribers_update_meta_processing();
				}
			}
			
			// On met à jour la date de dernière génération du flux auto
			sql_updateq(
				'spip_pushlists',
				array('date_feed' => $jourdhui),
				'id_pushlist = '.$id_pushlist
			);
		}
	}
	
	return 1;
}

/**
 * Programme l'envoi d'un push précis à une liste précise
 * 
 * Initialise tous les destinataires dans la table dédiée
 * 
 * TODO URGENT Pour l'instant on insère tout en bourrin d'un coup, mais il va falloir découper ensuite, car il peut y avoir beaucoup d'inscrits
 */
function pushsubscribers_pushlist_programmer_push($id_pushlist, $id_push) {
	// On va chercher tous les inscrits à cette liste, qui n'ont PAS déjà ce push programmé
	$destinataires = sql_allfetsel(
		'S.id_pushsubscriber, endpoint, auth, p256dh',
		'spip_pushsubscribers as P join spip_pushsubscriptions as S on S.id_pushsubscriber=P.id_pushsubscriber',
		array(
			'id_pushlist = '.intval($id_pushlist),
			'S.id_pushsubscriber not in (select id_pushsubscriber from spip_pushs_destinataires where id_push = '.intval($id_push).')',
		),
		'S.id_pushsubscriber',
		'S.maj ASC'
	);
	
	$now = date('Y-m-d H:i:s');
	$inserer = array();
	foreach ($destinataires as $destinataire) {
		$inserer[] = array(
			'id_push'             => $id_push,
			'id_pushsubscriber'   => $destinataire['id_pushsubscriber'],
			'endpoint'            => $destinataire['endpoint'],
			'p256dh'              => $destinataire['p256dh'],
			'auth'                => $destinataire['auth'],
			'date'                => $now,
			'statut'              => 'todo',
		);
	}
	
	if (!sql_insertq_multi('spip_pushs_destinataires', $inserer)) {
		foreach ($inserer as $i) {
			sql_insertq('spip_pushs_destinataires', $i);
			if (time() > PUSHSUBSCRIBERS_MAX_TIME) return;
		}
	}
}
