<?php
/**
 * Gestion du formulaire de de configuration de PushSubscribers
 *
 * @plugin     PushSubscribers
 * @copyright  2020
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Pushsubscribers\Formulaires
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Identifier le formulaire en faisant abstraction des paramètres qui ne représentent pas l'objet edité
 *
 * @param int|string $id_pushsubscriber
 *     Identifiant du pushsubscriber. 'new' pour un nouveau pushsubscriber.
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un pushsubscriber source d'une traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL du pushsubscriber, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_configurer_pushsubscribers_saisies_dist() {
	$saisies = array(
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'vapid',
				'label' => 'Clés d’identification du site',
				'attention' => 'Attention, tout changement de ces paramètres invalideront l’ensemble de vos inscriptions !',
			),
			'saisies' => array(
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'vapid_subject',
							'label' => 'Sujet VAPID',
							'defaut' => $GLOBALS['meta']['adresse_site'],
						),
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'vapid_public',
							'label' => 'Clé VAPID publique',
						),
					),
					array(
						'saisie' => 'input',
						'options' => array(
							'nom' => 'vapid_private',
							'label' => 'Clé VAPID privée',
							'inserer_fin' => '<input style="display:none;" class="submit" type="submit" value="Go" /><input class="link" name="vapid_generer" type="submit" value="Générer des clés VAPID" />'
						),
					),
			),
		),
		array(
			'saisie' => 'fieldset',
			'options' => array(
				'nom' => 'envois',
				'label' => 'Configuration des envois',
			),
			'saisies' => array(
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'heure_matin',
						'label' => 'Heure du matin',
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array(
							'min' => 0,
							'max' => 24,
						),
					),
				),
				array(
					'saisie' => 'input',
					'options' => array(
						'nom' => 'heure_soir',
						'label' => 'Heure du soir',
					),
					'verifier' => array(
						'type' => 'entier',
						'options' => array(
							'min' => 0,
							'max' => 24,
						),
					),
				),
			),
		),
	);
	
	return $saisies;
}

function formulaires_configurer_pushsubscribers_verifier_dist() {
	$erreurs = array();
	
	if (_request('vapid_generer')) {
		include_spip('inc/pushsubscribers');
		pushsubscribers_loader();
		
		$cles = Minishlink\WebPush\VAPID::createVapidKeys();
		
		if (isset($cles['publicKey']) and isset($cles['privateKey'])) {
			set_request('vapid_public', $cles['publicKey']);
			$erreurs['vapid_public'] = 'Cette clé vient d’être re-générée !';
			
			set_request('vapid_private', $cles['privateKey']);
			$erreurs['vapid_private'] = 'Cette clé vient d’être re-générée !';
			
			$erreurs['message_erreur'] = '';
		}
	}
	
	return $erreurs;
}
