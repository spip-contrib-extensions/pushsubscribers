
# Pushsubscribers

Gestion des inscriptions à des notifications Pushs et de leur envoi en masse.

## Architecture

Inspiré de l'architecture des plugins de Newsletters, cette fonctionnalité n'est pour l'instant pas découpé en plusieurs plugins différents.

Il gère donc dans le même plugin à la fois
- les inscrits (Pushsubscribers)
- les listes (Pushlists)
- les inscriptions à ces listes (Pushsubscriptions)
- les notifications à envoyer contenues dans ces listes (Pushs).

Ainsi que les mécanismes Javascript pour gérer les autorisations des navigateurs et garder en mémoire ce que les personnes acceptent de recevoir.

## Gestion des inscriptions

Les navigateurs permettent de recevoir des notifications en Push seulement si on a accepté d'en recevoir. Il faut donc demander l'autorisation aux gens. Mais les navigateurs ne gèrent que l'autorisation complète binaire, sans détailler si on ne veut que des notifications pour tel ou tel sujet, c'est seulement l'autorisation pour le site entier.

Il faut donc garder à la fois en mémoire les gens qui ont accepté d'avoir des notifications : ce sont les Pushsubscribers ; mais aussi le détail de ce qu'illes acceptent de recevoir : ce sont les Pushsubscriptions qui associent une personne inscrite à une Pushlist.

Inscrire une personne à une liste signifie qu'elle accepte de recevoir des notifications de ce "contexte", peu importe que ces notifications soient générées automatiquement ou manuellement. Cela se fait pour l'instant avec des cases à cocher ayant l'attribut "data-pushlist". Un modèle est fourni pour les générer comme il faut :
```
<push|inscription|label=Recevoir des notifications pour tous les nouveaux articles|pushlist=articles>
<push|inscription|label=Recevoir des notifications des articles avec le mot-clé Patate|pushlist=articles?id_mot=123>
<push|inscription|label=Recevoir des notifications des articles de la Première Rubrique|pushlist=articles?id_rubrique=1>
```

La Pushlist est identifié par son contexte, c'est-à-dire très concrètement ce que les gens acceptent de recevoir en étant inscrit à celle-ci. Ici c'est par exemple "articles?id_mot=123".

Si cette Pushlist n'existait pas déjà dans la base, elle sera créée à la volée, et la personne y sera inscrite. On peut donc parfaitement afficher dans ses squelettes des cases pour 1000 situations différentes (si on a 1000 mots-clés par exemple), sans pour autant avoir créé chacune de ces listes en amont !

## Gestion des listes

Une liste est donc identifiée de manière unique par son contexte. Deux listes ne peuvent avoir le même contexte.

Les listes peuvent être crées manuellement dans l'interface d'admin, ou à la volée lors des inscriptions par les cases à cocher. Cela permet donc de multiples utilisations, que ce soit pour envoyer une notification manuellement depuis l'interface d'admin à telle liste précise, ou par un flux automatique.

### Listes automatiques

Un mécanisme permet de décrire le titre et le contenu d'une liste sans avoir à la créer en amont dans l'interface d'admin. Il s'agit des "flux" (ou "feed").

On peut les définir de deux manières, en PHP ou en squelettes. Pour cela il faut une fonction PHP ou un squelettes SPIP ayant le nom donné dans les contextes de Pushlist, sans la query string. Ainsi plusieurs listes peuvent être servies par le même flux ayant des paramètres.

Par exemple pour les exemples de listes donnés précédemment, il faudra :
- soit un fichier PHP `pushs/articles.php` avec une fonction `pushs_articles_dist()`
- soit un squelette `pushs/articles.html`

Le contexte envoyé à la fonction ou au squelette est complété par la date de dernière génération du flux si elle existe, dans une clé `date_feed`.
Il est très fortement recommandé d'utiliser cette date dans vos requêtes, afin de ne générer alors que les nouveaux contenus qui sont apparus depuis cette date. Sinon plusieurs notifications de la même chose pourraient être générés (ça peut être voulu mais ça doit être un choix explicite).

Dans les deux cas il faut renvoyer un tableau, soit directement en PHP, soit en JSON si en squelette (remplir un tableau puis faire `[(#GET{flux}|json_encode)]` à la fin).

Le flux doit se présenter comme ceci :
``` php
array(
	'titre' => _T('titre de mon flux'),
	'pushs' => array(
		// Un push
		array(
			'titre' => 'Titre de ma notification',
			'texte' => 'Texte pouvant être un peu plus long, mais faut pas exagérer.',
			'url' => 'https://ou_aller_quand_on_clique_sur_la_notif',
			'date' => '2021-02-09 15:00:00' // Possiblement une date à laquelle on veut programmer, sinon sera envoyé dès que possible
			'topic' => 'article123', // identifiant unique ce max 32 caractères, permettant de ne pas envoyer plusieurs fois la même notification aux mêmes personnes
		),
	),
)
```

## Gestion des envois

Afin de gérer les envois côté serveur, le plugin utilise la librairie WebPush PHP : https://github.com/web-push-libs/web-push-php

Le plugin prévoit deux manières d'envoyer des notifications :
- on peut utiliser directement les fonctions d'envoi, notamment à l'unité, mais dans ce cas on ne garde pas de trace de ce qu'on a décidé d'envoyer
- on peut remplir la table des notifications Pushs, et dans ce cas, tous les pushs dedans seront programmés à envoyer, ce qui permet d'en prévoir de très nombreux à la fois, pour envoyer en masse.

## Installation

### Dépendance

Le plugin nécessite les plugins Saisies et Offline. Le plus gros point est l'installation et la configuration du plugin Offline, qui peut être assez complexe. Son dépôt et sa documentation sont pour l'instant ici : https://git.nursit.net/open/offline

### Configuration

Pour fonctionner, un site envoyeur de notifications doit fournir aux services d'envois (Mozilla, Google, etc) des informations VAPID d'authentification.

Dans le formulaire de configuration, un bouton permet de générer automatiquement ces informations. Attention ! Elles identifient de manière unique le site qui envoie, donc si ces informations changent, plus aucun abonnement ne sera valide ! Il faut donc veiller à ne plus y toucher ensuite (TODO : masquer ces champs dans le form de config lorsqu'ils sont déjà bien remplis).

### Tester

### Le service worker

Une fois tout installé, vous devriez voir le service worker détécté dans l'outil de développement du navigateur (dans Firefox, onglet Applications).

Dans n'importe quelle page, ajoutez un ou plusieurs appels au modèle d'inscription décrit précédemment. Vous aurez alors des cases à cocher pour vous inscrire.

Si le service worker fonctionne correctement, ces cases doivent passer de désactivées à utilisables.

### Une première inscription

Dès que vous cliquez sur l'une d'elles, la première fois le navigateur devrait vous demander l'autorisation de recevoir des notifications de ce site. Si l'autorisation est ok, alors cela devrait enregistrer vos préférences. Ainsi si vous revenez sur une page qui contient ces cases, alors elles devraient désormais être pré-cochées pour celles où vous avez déjà demandé à être inscrit⋅e.

Dans l'admin du site, allez dans "Activité => Inscrits aux notifications push". Vous devriez voir une première inscription, la votre. Les inscriptions ne sont pas liées à des gens, elles sont totalement anonymes, il n'est donc pas facile de savoir de qui il s'agit. Mais pour tester rapidement lors de la phase de mise en place, il n'y a que vous-mêmes.

Si vous allez sur la page d'admin de cette première inscription, vous aurez un bouton pour envoyer une notification de test. Vous devriez alors immédiatement recevoir une notification native avec un contenu de test. Ça marche !

### Flux automatique

Pour tester la génération automatique des flux, vous pouvez en créer un suivant la documentation qui précède, ou bien utiliser le flux fourni par défaut pour les articles.

Pour cela vous devez utiliser le modèle d'inscription encore, en lui demandant de s'inscrire à une liste donc l'idenfiant de contexte correspond au nom du flux à tester ("articles" pour celui fourni par défaut, comme dans les exemples plus haut).

Dès qu'il y a une inscription à une liste *qui est liée à un flux auto*, alors ça le détecte. La liste est considérée comme étant automatique, et un génie va venir vérifier régulièrement s'il y a des notifications push à générer à partir de son flux. Vous devriez voir ça dans la liste des tâches de l'admin de SPIP.

Si vous écrivez de nouveaux articles sur ce site, alors le flux par défaut va le détecter et créer des pushs correspondants **et** les programmer pour les destinataires inscrits à la liste en question. Au fur et à mesure des visites (puisque les tâches SPIP se lancent lors des hits PHP), les pushs vont être envoyés et vous devriez les recevoir.

## Remarques et problèmes rencontrés

- Le service worker de Offline est assez capricieux à se lancer, il faut vraiment faire attention :
	- il ne faut absolument aucun 404
	- il faut absolument être en HTTPS ou en localhost
	- en localhost donc juste HTTP sans vrai certificat, chez moi ça marche sur Chromium, mais sur Firefox, il faut le panneau de développement ouvert, avec l'option "Activer les SW en HTTP quand la boite est ouverte"
	- chez moi sur Firefox même avec le panneau de dev ouvert en localhost, si je suis en fenêtre privé pour tester en anonyme, le SW ne se lance pas (alors que sur Chromium oui)
