<?php
/**
 * Utilisations de pipelines par PushSubscribers
 *
 * @plugin     PushSubscribers
 * @copyright  2020
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Pushsubscribers\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/*
 * Des modifs supplémentaires après édition
 */
function pushsubscribers_pre_edition($flux) {
	// Si on modifie une liste
	if (isset($flux['args']['type']) and $flux['args']['type'] == 'pushlist') {
		$id_pushlist = intval($flux['args']['id_objet']);
		// L'état actuel
		$pushlist = sql_fetsel('*', 'spip_pushlists', 'id_pushlist = ' . $id_pushlist);
		
		// Si le contexte a été modifié
		if (isset($flux['data']['contexte']) and $contexte = $flux['data']['contexte']) {
			// On cherche si ce nouveau contexte a un flux prévu
			$fonction_pushlist = charger_fonction('pushlist', 'pushs');
			if ($feed = $fonction_pushlist($contexte)) {
				// Dans ce cas c'est forcément un type=feed
				$flux['data']['type'] = 'feed';
			}
			
			// S'il n'y avait pas de titre avant, on le remplit avec le titre du flux ou le contexte
			if (!$pushlist['titre']) {
				$flux['data']['titre'] = isset($feed['titre']) ? $feed['titre'] : $contexte;
			}
		}
	}
	
	return $flux;
}

/**
 * Optimiser la base de données
 *
 * Supprime les objets à la poubelle.
 *
 * @pipeline optimiser_base_disparus
 * @param  array $flux Données du pipeline
 * @return array       Données du pipeline
 */
function pushsubscribers_optimiser_base_disparus($flux) {
	sql_delete('spip_pushsubscribers', "statut='poubelle' AND maj < " . sql_quote(trim($flux['args']['date'], "'")));

	return $flux;
}

function pushsubscribers_insert_head($flux) {
	$config = lire_config('pushsubscribers');

	$flux .= '<script type="text/javascript">
	jQuery.spip.pushsubscribers = {
		subject : "' . $config['subject'] . '",
		vapid_public : "' . $config['vapid_public'] . '"
	};
</script>';

	if ($chemin_utils = find_in_path('javascript/pushsubscribers.utils.js')) {
		$flux .= '<script type="text/javascript" src="' . $chemin_utils . '"></script>';
	}
	
	return $flux;
}

function pushsubscribers_serviceworker_install_js($liste) {
	//$liste[] = 'javascript/pushsubscribers.install.js';
	
	return $liste;
}

function pushsubscribers_serviceworker_sw_js($liste) {
	$liste[] = 'javascript/pushsubscribers.sw.js';
	
	return $liste;
}

/**
 * Tache periodique d'envoi
 *
 * @param array $taches_generales
 * @return array
 */
function pushsubscribers_taches_generales_cron($taches_generales){
	// on active la tache cron d'envoi uniquement si necessaire (un envoi en cours)
	if (
		isset($GLOBALS['meta']['pushsubscribers_processing'])
	  and (
			$GLOBALS['meta']['pushsubscribers_processing'] === 'oui' 
			or $GLOBALS['meta']['pushsubscribers_processing'] < $_SERVER['REQUEST_TIME']
		)
	) {
		include_spip('inc/pushsubscribers');
		
		list($periode, $nb) = pushsubscribers_cadence();
		$taches_generales['pushsubscribers_bulksend'] = max(60, $periode - 15);
	}
	
	// Comme la syndic toutes les 1m30, on raffraichit UN flux seulement
	$taches_generales['pushsubscribers_pushlist'] = 90;

	return $taches_generales;
}
