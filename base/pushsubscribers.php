<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     PushSubscribers
 * @copyright  2020
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Pushsubscribers\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Déclaration des alias de tables et filtres automatiques de champs
 *
 * @pipeline declarer_tables_interfaces
 * @param array $interfaces
 *     Déclarations d'interface pour le compilateur
 * @return array
 *     Déclarations d'interface pour le compilateur
 */
function pushsubscribers_declarer_tables_interfaces($interfaces) {
	$interfaces['table_des_tables']['pushsubscribers'] = 'pushsubscribers';
	$interfaces['table_des_tables']['pushlists'] = 'pushlists';
	$interfaces['table_des_tables']['pushs'] = 'pushs';
	$interfaces['table_des_tables']['pushs_destinataires'] = 'pushs_destinataires';

	return $interfaces;
}


/**
 * Déclaration des objets éditoriaux
 *
 * @pipeline declarer_tables_objets_sql
 * @param array $tables
 *     Description des tables
 * @return array
 *     Description complétée des tables
 */
function pushsubscribers_declarer_tables_objets_sql($tables) {
	// Les inscrits, les gens qui ont accepté de recevoir des notifs de ce site
	$tables['spip_pushsubscribers'] = array(
		'type' => 'pushsubscriber',
		'principale' => 'oui',
		'field'=> array(
			'id_pushsubscriber'  => 'bigint(21) NOT NULL',
			'id_auteur'          => 'bigint(21) NOT NULL',
			'endpoint'           => 'varchar(512) NOT NULL DEFAULT ""',
			'p256dh'             => 'varchar(512) NOT NULL DEFAULT ""',
			'auth'               => 'varchar(512) NOT NULL DEFAULT ""',
			'vapid_public'       => 'varchar(512) NOT NULL DEFAULT ""',
			'date_creation'      => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'             => 'varchar(20)  DEFAULT "0" NOT NULL',
			'maj'                => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_pushsubscriber',
			'KEY statut'         => 'statut',
			'KEY id_auteur'      => 'id_auteur',
		),
		'titre' => 'concat(\'Pushsubscriber \', pushsubscribers.id_pushsubscriber) AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => array('endpoint', 'p256dh', 'auth', 'vapid_public'),
		'champs_versionnes' => array('endpoint', 'p256dh', 'auth', 'vapid_public', 'date_creation'),
		'rechercher_champs' => array("endpoint" => 5, "p256dh" => 5, "auth" => 5, "vapid_public" => 5),
		'tables_jointures'  => array(
			'id_pushsubscriber' => 'pushsubscriptions',
		),
		'statut_textes_instituer' => array(
			'valide'   => 'texte_statut_publie',
			'refuse'   => 'texte_statut_refuse',
			'poubelle' => 'texte_statut_poubelle',
		),
		'statut_images' => array(
			'valide'     => 'puce-publier-8.png',
			'refuse'     => 'puce-refuser-8.png',
			'poubelle'   => 'puce-supprimer-8.png',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'valide',
				'previsu'   => 'valide',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'pushsubscriber:texte_changer_statut_pushsubscriber',
	);
	
	// Les listes ou contextes que les gens peuvent accepter de recevoir
	$tables['spip_pushlists'] = array(
		'type' => 'pushlist',
		'principale' => 'oui',
		'field'=> array(
			'id_pushlist'        => 'bigint(21) NOT NULL',
			'contexte'           => 'varchar(255) NOT NULL DEFAULT ""', // le vrai identifiant de la liste : un mot et une query string éventuelle (articles?id_rubrique=123&id_mot=456)
			'titre'              => 'text NOT NULL DEFAULT ""',
			'auto'               => 'tinyint not null default "1"', // par défaut c'est auto, sauf si on édite par le form
			'type'               => 'varchar(25) NOT NULL DEFAULT ""', // "feed" pour dire que c'est un flux à lire régulièrement
			'date_feed'          => 'datetime not null default "0000-00-00 00:00:00"', // date de dernière lecture du feed
			'maj'                => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_pushlist',
		),
		'titre' => 'titre AS titre, "" AS lang',
		 #'date' => '',
		'champs_editables'  => array('titre', 'contexte', 'auto'),
		'champs_versionnes' => array('titre', 'type', 'contexte', 'auto'),
		'rechercher_champs' => array("titre" => 10),
		'tables_jointures'  => array(
			'id_pushlist' => 'pushsubscriptions',
		),
	);
	
	// Les notifs à envoyer
	$tables['spip_pushs'] = array(
		'type' => 'push',
		'principale' => 'oui',
		'field'=> array(
			'id_push'            => 'bigint(21) NOT NULL',
			'titre'              => 'text NOT NULL DEFAULT ""',
			'texte'              => 'text NOT NULL DEFAULT ""',
			'url'                => 'varchar(255) NOT NULL DEFAULT ""',
			'topic'              => 'varchar(32) NOT NULL DEFAULT ""',
			'date_debut'         => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"', // par défaut ça démarre au plus vite sauf si on a mis une date de début plus loin
			'date'               => 'datetime NOT NULL DEFAULT "0000-00-00 00:00:00"',
			'statut'             => 'varchar(20)  DEFAULT "processing" NOT NULL',
			'maj'                => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'
		),
		'key' => array(
			'PRIMARY KEY'        => 'id_push',
			'KEY statut'         => 'statut',
			'KEY topic'          => 'topic',
		),
		'titre' => 'titre AS titre, "" AS lang',
		'date' => 'date',
		'champs_editables'  => array('titre', 'texte', 'url', 'topic', 'date_debut'),
		'champs_versionnes' => array('titre', 'texte', 'url', 'topic', 'date_debut'),
		'rechercher_champs' => array("titre" => 10, "texte" => 5, "topic" => 10),
		'tables_jointures'  => array(),
		'statut_textes_instituer' => array(
			'prepa'      => 'texte_statut_en_cours_redaction',
			'prop'       => 'texte_statut_propose_evaluation',
			//~ 'publie'   => 'texte_statut_publie',
			'refuse'     => 'texte_statut_refuse',
			'init'       => 'push:texte_statut_init',
			'pause'      => 'push:texte_statut_pause',
			'processing' => 'push:texte_statut_processing',
			'end'        => 'push:texte_statut_end',
			'cancel'     => 'push:texte_statut_cancel',
			'poubelle'   => 'texte_statut_poubelle',
			'archive'    => 'push:texte_statut_archive',
		),
		'statut_images' => array(
			'prepa'      => 'puce-preparer-8.png',
			'prop'       => 'puce-proposer-8.png',
			'refuse'     => 'puce-refuser-8.png',
			'init'       => 'puce-planifier-8.png',
			'pause'      => 'puce-preparer-8.png',
			'processing' => 'puce-proposer-8.png',
			'end'        => 'puce-publier-8.png',
			'cancel'     => 'puce-refuser-8.png',
			'poubelle'   => 'puce-supprimer-8.png',
			'archive'    => 'puce-archiver-8.png',
		),
		'statut'=> array(
			array(
				'champ'     => 'statut',
				'publie'    => 'publie',
				'previsu'   => 'publie,prop,prepa',
				'post_date' => 'date',
				'exception' => array('statut','tout')
			)
		),
		'texte_changer_statut' => 'push:texte_changer_statut_push',
	);

	return $tables;
}

/**
 * Déclaration des tables secondaires (liaisons)
 * @param array $tables
 * @return array
 */
function pushsubscribers_declarer_tables_auxiliaires($tables) {
	// Les inscriptions c'est-à-dire ce que chaque inscrit a vraiment accepté de recevoir comme liste
	$tables['spip_pushsubscriptions'] = array(
		'field' => array(
			'id_pushsubscriber'   => 'bigint(21) default "0" NOT NULL',
			'id_pushlist'         => 'bigint(21) default "0" NOT NULL',
			'maj'                 => 'TIMESTAMP NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP',
		),
		'key' => array(
			"PRIMARY KEY"           => "id_pushsubscriber,id_pushlist",
			"KEY id_pushsubscriber" => "id_pushsubscriber",
			"KEY id_pushlist"       => "id_pushlist",
		)
	);
	
	// À qui envoyer telle notif précise
	$tables['spip_pushs_destinataires'] = array(
		'field' => array(
			"id_push"           => "bigint(21) DEFAULT '0' NOT NULL",
			"id_pushsubscriber" => "bigint(21) DEFAULT '0' NOT NULL",
			"date"              => "datetime NOT NULL DEFAULT '0000-00-00 00:00:00'",
			"statut"            => "char(4)  DEFAULT 'todo' NOT NULL", // todo, sent, fail
			"try"               => "tinyint NOT NULL DEFAULT 0", // nombre d'essais
			// tous les champs permettant d'envoyer sans refaire une requête sur spip_pushsubscribers
			'endpoint'          => 'varchar(512) NOT NULL DEFAULT ""',
			'p256dh'            => 'varchar(512) NOT NULL DEFAULT ""',
			'auth'              => 'varchar(512) NOT NULL DEFAULT ""',
		),
		'key' => array(
			"PRIMARY KEY" => "id_push,id_pushsubscriber",
			"KEY statut"  => "statut"
		)
	);

	return $tables;
}
