<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Cherche si un contexte donné a un flux prévu et renvoie son contenu
 * 
 * @param string $contexte
 *   Un mot et une query string éventuelle (articles?id_rubrique=123&id_mot=456)
 * @return array
 *   Tableau PHP représentant le flux
 */
function pushs_pushlist_dist($contexte) {
	// Par défaut, pas de flux du tout
	$flux = $chemin = null;
	
	$parse = parse_url($contexte);
	if (isset($parse['path'])) {
		$chemin = $parse['path'];
	}
	if (isset($parse['query'])) {
		parse_str($parse['query'], $contexte);
	}
	else {
		$contexte = array();
	}
	
	// Log
	spip_log("Vérification du flux : \"$chemin\", avec les paramètres :", 'pushs.'._LOG_INFO_IMPORTANTE);
	spip_log($contexte, 'pushs.'._LOG_INFO_IMPORTANTE);
	
	// S'il y a une fonction PHP pour ce flux, c'est prioritaire
	if ($chemin and $fonction_contexte = charger_fonction($chemin, 'pushs', true)) {
		$flux = $fonction_contexte($contexte);
	}
	// Sinon s'il y a un squelette et que ça retourne un JSON
	elseif (
		find_in_path("pushs/$chemin")
		and $json = recuperer_fond("pushs/$chemin", $contexte)
		and $json = json_decode($json, true)
		and is_array($json)
	) {
		$flux = $json;
	}
	
	return $flux;
}
