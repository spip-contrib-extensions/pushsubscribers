<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function pushs_articles_dist($contexte=array()) {
	include_spip('base/abstract_sql');
	include_spip('inc/filtres');
	include_spip('inc/texte');
	
	$flux = array(
		'titre' => _T('info_articles'),
		'pushs' => array(),
	);
	
	// Si jamais on veut filtrer
	$where = array(
		'statut = "publie"',
	);
	
	// S'il y a une date de dernière génération, on l'utilise
	$date_feed = isset($contexte['date_feed']) ? $contexte['date_feed'] : '0000-00-00 00:00:00';
	$where[] = 'date > '.sql_quote($date_feed);
	
	if (isset($contexte['id_rubrique'])) {
		$id_rubrique = intval($contexte['id_rubrique']);
		$flux['titre'] .= ' - ' . _T('rubrique') . ' ' . generer_info_entite($id_rubrique, 'rubrique', 'titre');
		
		$where[] = 'id_rubrique = '.$id_rubrique;
	}
	
	if ($articles = sql_allfetsel('id_article, titre, texte, date', 'spip_articles', $where, '', 'date desc', '0,20')) {
		foreach ($articles as $article) {
			$push = array(
				'titre' => $article['titre'],
				'texte' => couper($article['texte'], 80, '…'),
				'url' => url_absolue(generer_url_entite($article['id_article'], 'article', '', '', true)),
				'date' => $article['date'],
				'topic' => 'article'.$article['id_article'], // ne pas notifier plusieurs fois les gens pour un même article, même si ça vient de listes différentes
			);
			
			$flux['pushs'][] = $push;
		}
	}
	
	return $flux;
}
