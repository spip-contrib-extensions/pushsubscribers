<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_pushsubscriber' => 'Ajouter cet inscrit',

	// C
	'champ_auth_label' => 'Auth',
	'champ_date_creation_label' => 'Date de création',
	'champ_endpoint_label' => 'Endpoint',
	'champ_id_auteur_label' => 'Compte lié',
	'champ_p256dh_label' => 'p256dh',
	'champ_vapid_public_label' => 'Clé VAPID publique',
	'confirmer_supprimer_pushsubscriber' => 'Confirmez-vous la suppression de cet inscrit aux notifications push ?',

	// I
	'icone_creer_pushsubscriber' => 'Créer une inscription',
	'icone_modifier_pushsubscriber' => 'Modifier cet inscrit',
	'info_1_pushsubscriber' => 'Un inscrit aux notifications push',
	'info_aucun_pushsubscriber' => 'Aucun inscrit aux notifications push',
	'info_nb_pushsubscribers' => '@nb@ inscrits aux notifications push',
	'info_pushsubscribers_auteur' => 'Les inscriptions aux notifications push de cet auteur',

	// R
	'retirer_lien_pushsubscriber' => 'Retirer cet inscrit',
	'retirer_tous_liens_pushsubscribers' => 'Retirer tous les inscrits',

	// S
	'supprimer_pushsubscriber' => 'Supprimer cet inscrit',

	// T
	'texte_ajouter_pushsubscriber' => 'Ajouter une inscription',
	'texte_changer_statut_pushsubscriber' => 'Ce inscrit aux notifications push est :',
	'texte_creer_associer_pushsubscriber' => 'Créer et associer un inscrit aux notifications push',
	'texte_definir_comme_traduction_pushsubscriber' => '',
	'titre_langue_pushsubscriber' => 'Langue de cet inscrit',
	'titre_logo_pushsubscriber' => 'Logo de ce inscrit aux notifications push',
	'titre_objets_lies_pushsubscriber' => 'Liés à cet inscrit',
	'titre_page_pushsubscribers' => 'Les inscrits aux notifications push',
	'titre_pushsubscriber' => 'Inscrit aux notifications push',
	'titre_pushsubscriber_court' => 'Inscrit',
	'titre_pushsubscribers' => 'Inscrits aux notifications push',
	'titre_pushsubscribers_rubrique' => 'Inscrits aux notifications push de la rubrique',
);
