<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'pushsubscribers_description' => 'Ce plugin fournit les outils permettant d\'inscrire un navigateur a un ou plusieurs flux de notifications pushs, et les fonctions d\'envoi vers les inscrits.',
	'pushsubscribers_nom' => 'PushSubscribers',
	'pushsubscribers_slogan' => 'Gère les inscriptions à des notifications push',
);
