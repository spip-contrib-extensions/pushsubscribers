<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_pushlist' => 'Ajouter cette liste de notifications push',

	// C
	'champ_contexte_explication' => 'Le contexte est ce que les gens vont autoriser à recevoir comme notifications avec cette liste. Cela peut être un simple identifiant comme "commande" ou "admin", ou l’URL d’un flux de notifications (voir la documentation).',
	'champ_contexte_label' => 'Contexte',
	'champ_titre_label' => 'Titre',
	'champ_type_label' => 'Type de liste',
	'champ_type_feed_label' => 'Cette liste est liée à un flux de contenus automatique',
	'confirmer_supprimer_pushlist' => 'Confirmez-vous la suppression de cette liste de notifications push ?',

	// I
	'icone_creer_pushlist' => 'Créer une liste de notifications push',
	'icone_modifier_pushlist' => 'Modifier cette liste de notifications push',
	'info_1_pushlist' => 'Une liste de notifications push',
	'info_aucun_pushlist' => 'Aucune liste de notifications push',
	'info_nb_pushlists' => '@nb@ listes de notifications push',
	'info_pushlists_auteur' => 'Les listes de notifications push de cet auteur',
	'inscriptions' => 'Inscriptions aux listes',
	
	// R
	'retirer_lien_pushlist' => 'Retirer cette liste de notifications push',
	'retirer_tous_liens_pushlists' => 'Retirer toutes les listes de notifications push',

	// S
	'supprimer_pushlist' => 'Supprimer cette liste de notifications push',

	// T
	'texte_ajouter_pushlist' => 'Ajouter une liste de notifications push',
	'texte_changer_statut_pushlist' => 'Cette liste de notifications push est :',
	'texte_creer_associer_pushlist' => 'Créer et associer une liste de notifications push',
	'texte_definir_comme_traduction_pushlist' => 'Cette liste de notifications push est une traduction de la liste de notifications push numéro :',
	'titre_langue_pushlist' => 'Langue de cette liste de notifications push',
	'titre_logo_pushlist' => 'Logo de cette liste de notifications push',
	'titre_objets_lies_pushlist' => 'Liés à cette liste de notifications push',
	'titre_page_pushlists' => 'Les listes de notifications push',
	'titre_pushlist' => 'Liste de notifications push',
	'titre_pushlists' => 'Listes de notifications push',
	'titre_pushlists_rubrique' => 'Listes de notifications push de la rubrique',
);
