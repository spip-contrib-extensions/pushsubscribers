<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_push' => 'Ajouter cette notification push',

	// C
	'champ_date_label' => 'Date',
	'champ_date_debut_label' => 'Date d’envoi',
	'champ_date_debut_vide' => 'Au plus vite',
	'champ_texte_label' => 'Texte',
	'champ_topic_label' => 'Étiquette identifiant',
	'champ_titre_label' => 'Titre',
	'champ_url_label' => 'URL',
	'confirmer_supprimer_push' => 'Confirmez-vous la suppression de cette notification push ?',
	
	// E
	'envois' => 'Envois',
	'envois_1_fail' => '1 envoi échoué',
	'envois_1_sent' => '1 envoi réussi',
	'envois_1_todo' => '1 envoi en cours',
	'envois_nb_essais' => 'Nombre d’essais',
	'envois_nb_fail' => '@nb@ envois échoués',
	'envois_nb_sent' => '@nb@ envois réussis',
	'envois_nb_todo' => '@nb@ envois en cours',
	
	// I
	'icone_creer_push' => 'Créer une notification push',
	'icone_modifier_push' => 'Modifier cette notification push',
	'info_1_push' => 'Une notification push',
	'info_aucun_push' => 'Aucune notification push',
	'info_nb_pushs' => '@nb@ notifications push',
	'info_pushs_auteur' => 'Les notifications push de cet auteur',

	// R
	'retirer_lien_push' => 'Retirer cette notification push',
	'retirer_tous_liens_pushs' => 'Retirer toutes les notifications push',

	// S
	'supprimer_push' => 'Supprimer cette notification push',

	// T
	'texte_ajouter_push' => 'Ajouter une notification push',
	'texte_changer_statut_push' => 'Cette notification push est :',
	'texte_creer_associer_push' => 'Créer et associer une notification push',
	'texte_definir_comme_traduction_push' => 'Cette notification push est une traduction de la notification push numéro :',
	'texte_statut_archive' => 'archivé',
	'texte_statut_cancel' => 'annulé',
	'texte_statut_end' => 'envoi terminé',
	'texte_statut_init' => 'planifié',
	'texte_statut_pause' => 'en pause',
	'texte_statut_processing' => 'en cours d’envoi',
	'titre_langue_push' => 'Langue de cette notification push',
	'titre_logo_push' => 'Logo de cette notification push',
	'titre_objets_lies_push' => 'Liés à cette notification push',
	'titre_page_pushs' => 'Les notifications push',
	'titre_push' => 'Notification push',
	'titre_pushs' => 'Notifications push',
	'titre_pushs_rubrique' => 'Notifications push de la rubrique',
);
