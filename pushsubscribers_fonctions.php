<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function push_stats($id_push) {
	$stats = array(
		'total' => 0,
		'sent' => 0,
		'fail' => 0,
	);
	
	if ($id_push = intval($id_push)) {
		if ($total = sql_countsel('spip_pushs_destinataires', 'id_push='.$id_push)) {
			$stats['total'] = $total;
			
			// On ne fait les autres requêtes que s'il y a au moins un total
			if ($sent = sql_countsel('spip_pushs_destinataires', array('id_push='.$id_push, 'statut="sent"'))) {
			$stats['sent'] = $sent;
			}
			if ($fail = sql_countsel('spip_pushs_destinataires', array('id_push='.$id_push, 'statut="fail"'))) {
				$stats['fail'] = $fail;
			}
		}
	}
	
	return $stats;
}
