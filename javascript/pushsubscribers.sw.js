var offlineConfig;

/**
* Gestion d'une réception de push
*/
self.addEventListener('push', function (event) {
	console.log('Hey mon ami⋅e, tu reçois un push !', event);
	
	// On récupère le contenu du payload du push : une chaine json
	var params;
	if (event.data) {
		params = event.data.text();
		params = JSON.parse(params);
	}

	// On rajoute la date d'arrivée
	params.data.date_reception = Date.now();

	//~ var options = {
		//~ body: params['message'] ||'',
		//~ icon: params['icon'] || '',
		//~ // vibrate: params['vibrate'],
		//~ data: {
			//~ dateOfArrival: Date.now(),
			//~ url: params['url'] || '',
		//~ },
		//~ actions: [
			//~ {action: 'close', title: 'Fermer'}
		//~ ]
	//~ };

	// Keep the service worker alive until the notification is created
	event.waitUntil(
		self.registration.showNotification(params['title'], params)
	);
});

/**
* Gestion des clics dans les notifications
*/
self.addEventListener('notificationclick', function (event) {
	var notification = event.notification;
	var action = event.action;
	var url;

	if (notification.data.url) {
		url = notification.data.url;
	}
	else {
		url = '/';
	}

	if (action === 'close') {
		notification.close();
	}
	else {
		event.waitUntil(
			clients.openWindow(url)
		);
	}
});

/**
 * Gestion de l'expiration de l'inscription
 */
//~ self.addEventListener('pushsubscriptionchange', function(event) {
  //~ console.log('Inscription aux notifications expirée !');
  
  //~ event.waitUntil(
    //~ self.registration.pushManager.subscribe({ userVisibleOnly: true })
    //~ .then(function(subscription) {
      //~ console.log('Subscribed after expiration', subscription.endpoint);
      //~ return fetch('register', {
        //~ method: 'post',
        //~ headers: {
          //~ 'Content-type': 'application/json'
        //~ },
        //~ body: JSON.stringify({
          //~ endpoint: subscription.endpoint
        //~ })
      //~ });
    //~ })
  //~ );
//~ });
