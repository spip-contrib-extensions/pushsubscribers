var offlineConfig;

// Public base64 to Uint
function urlBase64ToUint8Array(base64String) {
	var padding = '='.repeat((4 - base64String.length % 4) % 4);
	var base64 = (base64String + padding)
			.replace(/\-/g, '+')
			.replace(/_/g, '/');

	var rawData = window.atob(base64);
	var outputArray = new Uint8Array(rawData.length);

	for (var i = 0; i < rawData.length; ++i) {
			outputArray[i] = rawData.charCodeAt(i);
	}
	return outputArray;
}

if (typeof offlineConfig == "undefined"){
	console.error("Erreur offline.install.js appele sans configuration");
}
else {
	// On appelle jQuery
	(function ($) {
		/**
		 * Chercher tous les boutons d'enregistrement aux pushs, et pour chacun activer le mécanisme
		 */
		function pushsubscribers_boutons() {
			// Si ça gère les serviceworkers et qu'on trouve la clé publique
			if ('serviceWorker' in navigator && jQuery.spip.pushsubscribers.vapid_public) {
				
				// On recherche l'enregistrement du SW
				navigator.serviceWorker.ready.then(function (registration) {
				// navigator.serviceWorker.getRegistration().then(function (registration) {
				// navigator.serviceWorker.register(offlineConfig.swUrl, offlineConfig.swOptions).then(function (registration) {
					var applicationServerKey = urlBase64ToUint8Array(jQuery.spip.pushsubscribers.vapid_public);
					var pushsubscriptions = null;
					
					// On va chercher la liste complète des contextes déjà acceptés par ce client web
					// histoire de tout avoir en une seule requête
					registration.pushManager.getSubscription()
						.then(function (subscription) {
							
						});
					
					//~ // On cherche tous les champs pour s'inscrire
					//~ $('[data-pushsubscription]:disabled').each(function() {
						//~ var me = $(this);
						//~ var contexte = me.data('pushsubscription');
						
						//~ // On va chercher si la personne a déjà accepté ce contexte
						
						
						//~ $(this).prop('disabled', false);
					//~ });
					//~ return registration.pushManager.getSubscription()
						//~ .then(subscription => {
							 //~ if (subscription) {
									 //~ return subscription;
							 //~ }
							 //~ return registration.pushManager.subscribe({
											//~ userVisibleOnly: true,
											//~ applicationServerKey: applicationServerKey
							 //~ });
						//~ })
						//~ .then(subscription => {
							//~ // On a maintenant une inscription, on l'envoie au serveur pour la garder en mémoire
							//~ subscription_json = subscription.toJSON();
							//~ console.debug("subscription_json", subscription_json);
							
							//~ // On envoie les infos à SPIP
							//~ $.post(
								//~ 'pushsubscription.api/create',
								//~ subscription_json
							//~ );
						//~ })
						//~ .catch(err => console.error('Impossible de souscrire aux notifications : ', err));
				}, function (err) {
					// registration failed :(
					console.log('Impossible de trouver l’enregistrement du Service Worker : ', err);
				});
			}
		}
		
		// Quand le DOM est prêt
		$(function() {
			pushsubscribers_boutons();
			// Et dès qu'il y a un chargement ajax
			onAjaxLoad('pushsubscribers_boutons');
		});
	}(jQuery));
}
