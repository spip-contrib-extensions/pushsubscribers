
var pushsubscribers = {
	// Public base64 to Uint
	urlBase64ToUint8Array: function (base64String) {
		var padding = '='.repeat((4 - base64String.length % 4) % 4);
		var base64 = (base64String + padding)
				.replace(/\-/g, '+')
				.replace(/_/g, '/');

		var rawData = window.atob(base64);
		var outputArray = new Uint8Array(rawData.length);

		for (var i = 0; i < rawData.length; ++i) {
				outputArray[i] = rawData.charCodeAt(i);
		}
		return outputArray;
	},
	
	// Avoir la bonne URL de l'API qu'on soit en admin ou pas
	get_url_api: function () {
		var url_api = 'pushsubscription.api/';
		if (typeof spip_ecrire !== 'undefined' && spip_ecrire) {
			url_api = '../' + url_api;
		}
		
		return url_api;
	},
	
	// Pré-cocher les champs d'inscription si on trouve, et activer l'action lors de leur clic
	activer_champs_inscription: function () {
		// Si ça gère les serviceworkers et qu'on trouve la clé publique
		if ('serviceWorker' in navigator && jQuery.spip.pushsubscribers.vapid_public) {
			// On recherche l'enregistrement du SW
			navigator.serviceWorker.ready.then(function (registration) {
				var applicationServerKey = pushsubscribers.urlBase64ToUint8Array(jQuery.spip.pushsubscribers.vapid_public);
				
				// On cherche si la personne a déjà accepté les pushs un jour pour ce site
				registration.pushManager.getSubscription()
					.then(function (subscription) {
						var pushsubscriptions = {};
						var url_api = pushsubscribers.get_url_api();
						
						// Si la personne a une inscription, on va chercher la liste complète des listes déjà acceptées
						// histoire de tout avoir en une seule requête
						if (subscription) {
							subscription_json = subscription.toJSON();
							
							$.ajax({
								url: url_api + 'list',
								data: subscription_json,
								type: 'GET',
								async: false,
								dateType: 'json', // on attend du json pour cette action là
								success: function (reponse) {
									if (reponse) {
										pushsubscriptions = reponse;
									}
								},
							});
						}
						
						// Qu'elle ait déjà une inscription ou pas
						// On cherche tous les champs pour s'inscrire pas encore activés
						$('[data-pushlist]:disabled').each(function() {
							var me = $(this);
							var contexte = me.data('pushlist');
							
							// On va chercher si la personne a déjà accepté ce contexte
							// = si on trouve ce contexte dans les inscriptions
							if (typeof pushsubscriptions[contexte] !== 'undefined') {
								me.prop('checked', true);
							}
							// Sinon on s'assure que c'est bien décoché
							else {
								me.prop('checked', false);
							}
							
							// Dans tous les cas, on gère le clic
							me.change(pushsubscribers.switch_inscription);
							
							// Et on permet d'utiliser le champ
							me.prop('disabled', false);
						});
					});
			});
		}
	},

	// Inscrire ou désinscrire à une liste précise lors du switch coché/décoché
	switch_inscription: function() {
		var champ = $(this);
		var pushlist = champ.data('pushlist');
		var checked = champ.is(':checked') ? 1 : 0;
		
		// À chaque clic, on va retester si on trouve une inscription
		navigator.serviceWorker.ready.then(function (registration) {
			var applicationServerKey = pushsubscribers.urlBase64ToUint8Array(jQuery.spip.pushsubscribers.vapid_public);
			
			// On cherche si la personne a déjà accepté les pushs un jour pour ce site
			registration.pushManager.getSubscription()
				.then(function (subscription) {
					// Si déjà inscrit super
					if (subscription) {
						return subscription;
					}
					// Sinon on demande l'autorisation générale NOW (ça veut dire maintenant, en anglais)
					return registration.pushManager.subscribe({
						userVisibleOnly: true,
						applicationServerKey: applicationServerKey,
					});
				})
				// Là on est vraiment sûr d'avoir une inscription, soit passée soit qui vient d'être donnée
				.then(function (subscription) {
					var subscription_json = subscription.toJSON();
					var url_api = pushsubscribers.get_url_api();
					//~ console.debug("subscription_json", subscription_json);
					
					// En plus de créer ou mettre à jour l'inscription général, il faut inscrire ou désinscrire à la liste précise
					subscription_json.pushlist = pushlist;
					subscription_json.pushlist_checked = checked;
					
					$.post(
						url_api + 'create',
						subscription_json
					);
				})
				.catch(function(error) {
					console.error('Impossible de souscrire aux notifications : ', error)
				});
		});
	}
};

// On appelle jQuery
(function ($) {
	// Quand le DOM est prêt
	$(function() {
		pushsubscribers.activer_champs_inscription();
		// Et dès qu'il y a un chargement ajax (pas pour l'instant car boucle infinie)
		//~ onAjaxLoad(pushsubscribers.activer_champs_inscription);
	});
}(jQuery));
