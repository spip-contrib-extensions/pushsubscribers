<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('base/abstract_sql');
include_spip('action/editer_objet');
include_spip('inc/autoriser');
include_spip('inc/config');
include_spip('inc/session');
include_spip('inc/pushsubscribers');

function action_api_pushsubscription_dist() {
	// On récupère les infos
	$action = _request('arg');
	$endpoint = _request('endpoint');
	$keys = _request('keys');
	$auth = $keys['auth'];
	$p256dh = $keys['p256dh'];
	$pushlist = html_entity_decode(_request('pushlist'));
	$pushlist_checked = ($pushlist and _request('pushlist_checked'));
	$vapid_public = lire_config('pushsubscribers/vapid_public');
	
	if (
		in_array($action, array('create', 'update', 'delete', 'list'))
		and $endpoint and $auth and $p256dh
	) {
		// On cherche si le endpoint existe déjà
		$id_pushsubscriber = intval(sql_getfetsel('id_pushsubscriber', 'spip_pushsubscribers', 'endpoint = '.sql_quote($endpoint)));
		
		// Si c'est une demande de création ou update, c'est un peu pareil
		if (in_array($action, array('create', 'update'))) {
			// S'il n'existait pas déjà, on le crée
			if (!$id_pushsubscriber) {
				$id_pushsubscriber = objet_inserer('pushsubscriber', 0, array('date_creation'=>date('Y-m-d H:i:s')));
			}
			
			$maj = array(
				'endpoint' => $endpoint,
				'auth' => $auth,
				'p256dh' => $p256dh,
				'vapid_public' => $vapid_public,
				'statut' => 'valide',
			);
			if ($id_auteur = intval(session_get('id_auteur'))) {
				$maj['id_auteur'] = $id_auteur;
			}
			
			// On met à jour
			autoriser_exception('instituer', 'pushsubscriber', $id_pushsubscriber, true);
			$reponse = objet_modifier('pushsubscriber', $id_pushsubscriber, $maj);
			autoriser_exception('instituer', 'pushsubscriber', $id_pushsubscriber, false);
			
			// Ensuite on met à jour pour la liste précise (ça crée la liste à la volée si elle n'existait pas)
			if ($pushlist) {
				pushsubscribers_switch_pushlist($id_pushsubscriber, $pushlist, $pushlist_checked);
			}
		}
		// Si c'est pour supprimer
		elseif ($action == 'delete' and $id_pushsubscriber) {
			autoriser_exception('instituer', 'pushsubscriber', $id_pushsubscriber, true);
			$reponse = objet_modifier(
				'pushsubscriber', $id_pushsubscriber,
				array(
					'statut' => 'poubelle',
				)
			);
			autoriser_exception('instituer', 'pushsubscriber', $id_pushsubscriber, false);
		}
		// Si c'est pour chercher la liste des inscriptions
		elseif ($action == 'list' and $id_pushsubscriber) {
			$liste = array();
			
			// On cherche la liste complète de ses inscriptions
			if ($pushsubscriptions = sql_allfetsel('*', 'spip_pushsubscriptions', 'id_pushsubscriber = '.$id_pushsubscriber)) {
				foreach ($pushsubscriptions as $pushsubscription) {
					// Si on trouve bien la liste avec son contexte
					if ($contexte = sql_getfetsel('contexte', 'spip_pushlists', 'id_pushlist = '.intval($pushsubscription['id_pushlist']))) {
						$liste[$contexte] = $pushsubscription;
					}
				}
			}
			
			// On renvoie la liste en JSON
			header('Content-Type: application/json');
			echo json_encode($liste);
		}
		
		return array($id_pushsubscriber, $reponse);
	}
}

