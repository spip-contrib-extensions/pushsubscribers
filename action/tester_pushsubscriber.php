<?php
/**
 * Utilisation de l'action supprimer pour l'objet pushsubscriber
 *
 * @plugin     PushSubscribers
 * @copyright  2020
 * @author     Mukt
 * @licence    GNU/GPL
 * @package    SPIP\Pushsubscribers\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Action pour tester un·e pushsubscriber
 *
 * Vérifier l'autorisation avant d'appeler l'action.
 *
 * @param null|int $arg
 *     Identifiant à supprimer.
 *     En absence de id utilise l'argument de l'action sécurisée.
**/
function action_tester_pushsubscriber_dist($arg=null) {
	$need_confirm = false;
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	$id_pushsubscriber = intval($arg);

	// Si on peut tester l'envoi d'un push
	if ($id_pushsubscriber and autoriser('tester', 'pushsubscriber', $id_pushsubscriber)) {
		include_spip('inc/pushsubscribers');
		
		$retour = pushsubscribers_envoyer_notification(
			$id_pushsubscriber,
			array(
				'title' => 'Notification de test du site '.$GLOBALS['meta']['nom_site'],
				'body' => 'Ceci est juste une notification de test',
				'actions' => array(
					array('action' => 'close', 'title' => 'Fermer'),
				),
				'data' => array(
					'url' => $GLOBALS['meta']['adresse_site'],
				),
			)
		);
		
		// Erreur
		if ($retour) {
			include_spip('inc/minipres');
			echo minipres(
				'Erreur durant l’envoi de la notification de test',
				$retour
			);
			exit;
		}
	}
}
