<?php
/**
 * Produire les JS de gestion du offline
 *
 * @plugin     Offline
 * @copyright  2018
 * @author     Cedric
 * @licence    GNU/GPL
 * @package    SPIP\Offline\Action
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param null $arg
 * @param bool $return
 * @param null $cached_or_refresh
 *   par defaut null car si un fichier existe il sera pris par le .htaccess et donc on arrive la que par appel explicite
 * @return string
 */
function action_api_offline_dist($arg = null, $return = false, $cached_or_refresh = null) {
	if (is_null($arg)) {
		$arg = _request('arg');
	}

	if ($cached_or_refresh === 'cache'
	  and file_exists($f = _DIR_VAR . 'offline/' . $arg)) {
		lire_fichier($f, $t);
	}
	else {
		include_spip('inc/offline');

		$quoi = preg_replace(',\W+,', '_', $arg);
		$api_offline_quoi = charger_fonction('api_offline_' . $quoi, 'action');
		$t = $api_offline_quoi($cached_or_refresh === 'refresh');

		// si on est pas en mode debug on cache le fichier qui sera servi ensuite directement par apache
		// via regexp
		if (!defined('_OFFLINE_DEBUG') or !_OFFLINE_DEBUG) {
			// secu : on n'ecrit que des fichiers de la forme xxxxx.yyy
			if (preg_match(",^\w+\.(\w+)$,", $arg, $m)) {
				if (!function_exists('minifier')) {
					include_spip('compresseur_fonctions');
				}
				$t = minifier($t, $m[1]);
				$t = str_replace("\n}","}", $t);
				$t = str_replace("\n.",".", $t);
				$t = str_replace("\n;",";", $t);
				$t = str_replace(",\n",",", $t);
				$t = str_replace(";\n",";", $t);
				$t = str_replace("{\n","{", $t);
				$t = str_replace("(\n","(", $t);
				$t = str_replace("[\n","[", $t);
				$cache_file = sous_repertoire(_DIR_VAR, 'offline') . $arg;
				offline_ecrire_fichier_statique_versionne($cache_file, $t);
			}
		}
	}

	if ($return) {
		return $t;
	}

	$c = $GLOBALS['meta']['charset'];
	$content_type = 'text/plain';
	if (substr($arg,-3) === '.js') {
		$content_type = 'application/javascript';
	}
	elseif (substr($arg,-5) === '.json') {
		$content_type = 'application/json';
	}
	header('Content-Type: ' . $content_type . '; charset=' . $c);

	echo $t;
}


function action_api_offline_install_js_dist($force_refresh = false) {
	$config = offline_config_js($force_refresh);
	$debug = false;
	
	if (isset($config['debug']) && $config['debug']) {
		$debug = true;
		$c = $config;
	}
	else {
		// le offlineConfig du corps de page n'est pas partage avec celui du service worker
		// donc hors debug on y envoie que ce qui est utilise par l'installation
		$c = array(
			"swUrl" => $config["swUrl"],
			"swOptions" => $config["swOptions"],
			"cacheName" => $config["cacheName"],
		);
	}
	
	$serviceworker_install_js = pipeline(
		'serviceworker_install_js',
		array(
			'javascript/offline.install.utils.js',
			$debug ? 'javascript/offline.install.debug.js' : '',
			'javascript/offline.install.js',
		)
	);
	
	return offline_build_jslist($c, $serviceworker_install_js);
}

function action_api_offline_sw_js_dist($force_refresh = false) {
	$config = offline_config_js($force_refresh);
	
	$serviceworker_sw_js = pipeline(
		'serviceworker_sw_js',
		array(
			'javascript/offline.sw.utils.js',
			'javascript/offline.sw.caching.js',
			'javascript/offline.sw.manager.js',
			'javascript/offline.sw.js',
		)
	);
	
	return offline_build_jslist($config, $serviceworker_sw_js);
}

function action_api_offline_uninstall_js_dist($force_refresh = false) {
	$config = array();
	
	$serviceworker_uninstall_js = pipeline(
		'serviceworker_uninstall_js',
		array(
			'javascript/offline.uninstall.js',
		)
	);
	
	return offline_build_jslist($config, $serviceworker_uninstall_js);
}

function action_api_offline_urlsdownload_json_dist() {
	$objet = _request('objet');
	$id_objet = _request('id_objet');

	$file_urls_load = offline_filename_urls_to_load_objet($objet, $id_objet);
	if (!file_exists($file_urls_load) or !$urls = file_get_contents($file_urls_load)) {
		return '[]';
	}
	$urls = explode("\n", $urls);
	
	return json_encode($urls);
}
